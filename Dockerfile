FROM python:3.7 as base
RUN mkdir /app
COPY *.toml /app/
COPY *.lock /app/
WORKDIR /app
RUN pip3 install poetry \
    && poetry config virtualenvs.create false \
    && poetry install --no-dev

FROM base as development
COPY . /app/
EXPOSE 5000/tcp
RUN poetry install
CMD ["poetry", "run", "flask", "run", "-h", "0.0.0.0"]

FROM base as production
COPY . /app
EXPOSE 8000/tcp
CMD ["poetry", "run", "gunicorn", "-b", "0.0.0.0", "todo_app.app:create_app()"]

FROM base AS test
COPY . /app
RUN poetry install
CMD ["poetry", "run", "pytest"]
