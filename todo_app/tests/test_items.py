import os, pytest, requests
from todo_app import app
from dotenv import load_dotenv, find_dotenv
from todo_app.data.view_model import ViewModel
from todo_app.data.todo_items import Item

#Item class test
def test_item():
    to_do_item = Item("123", "111", "HelloTest123", "desc", "2022-01-01", "In Progress")
    assert to_do_item.id == "123"
    assert to_do_item.name == "HelloTest123"
    assert to_do_item.status == "In Progress"