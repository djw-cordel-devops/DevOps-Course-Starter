import os, pytest, requests
from todo_app import app
from dotenv import load_dotenv, find_dotenv
from todo_app.data.view_model import ViewModel
from todo_app.data.todo_items import Item

#ViewModel class tests
def test_to_do_list():
    view_model_data = [Item("123", "111", "HelloTest123", "desc", "2022-01-01", "In Progress")]
    test_view = ViewModel(view_model_data)
    assert test_view.items[0].id  == "123"
    assert test_view.items[0].short_id  == "111"
    assert test_view.items[0].status  == "In Progress"