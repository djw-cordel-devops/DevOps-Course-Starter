import os, pytest, requests
from todo_app import app
from dotenv import load_dotenv, find_dotenv
from todo_app.data.view_model import ViewModel
from todo_app.data.todo_items import Item

# Use our test integration config instead of the 'real' version
@pytest.fixture
def client():
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)
    # Create the new app.
    test_app = app.create_app()
    # Use the app to create a test_client that can be used in our tests.
    with test_app.test_client() as client:
        yield client

# This replaces any call to requests.get with our own function
def test_index_page(monkeypatch, client):
    monkeypatch.setattr(requests, 'get', stub)
    response = client.get('/')
class StubResponse():
    def __init__(self, fake_response_data):
        self.fake_response_data = fake_response_data

    def json(self):
        return self.fake_response_data

def stub(url, params={}):
    test_board_id = os.environ.get('TRELLO_BOARD_ID')    
    if url == f'https://api.trello.com/1/boards/{test_board_id}/lists':
        fake_response_data = [{
            'id': '123abc',
            'idShort': '123',
            'name': 'To Do',
            'desc': 'blah',
            'due': '2022-01-01',
            'cards': [{'id': '456', 'idShort': '123', 'name': 'Test card', 'desc': 'blah', 'due': '2022-02-02'}]
        }]
        return StubResponse(fake_response_data)
    raise Exception(f'Integration test did not expect URL "{url}"')