from flask import Flask, render_template, request, redirect, url_for
from todo_app.data.trello_items import get_items, add_item, item_in_progress, item_completed, reset_item_status
from todo_app.flask_config import Config
from todo_app.data.view_model import ViewModel

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config())

    @app.route('/')
    def index():
        """
        Returns the list of items.
        """
        items = get_items()
        return render_template('index.html', view_model=ViewModel(get_items()))
        
    @app.route('/item', methods = ['POST'])
    def add_new_item():
        """
        Returns the list of saved todo items from Trello web app. Redirects the user back to the index page

        """

        title = request.form.get("title")
        desc = request.form.get("desc text")
        due = request.form.get("date")
        add_item(title, desc, due)
        return redirect(url_for('index'))


    @app.route('/complete', methods = ['POST'])
    def set_item_to_complete():
        """
        Marks an item as completed. Redirects the user back to the index page

        """
        completed_item = request.form.get("mark as completed")
        item_completed(completed_item)
        return redirect(url_for('index'))


    @app.route('/inprogress', methods = ['POST'])
    def set_item_to_progress():
        """
        Sets an item as in progresss. Redirects the user back to the index page

        """
        in_progress = request.form.get("set to progress")
        item_in_progress(in_progress)
        return redirect(url_for('index'))


    @app.route('/reset', methods = ['POST'])
    def set_item_status():
        """
        Resets an item status.

        """
        reset_item = request.form.get("Reset item status")
        reset_item_status(reset_item)
        return redirect(url_for('index'))

    return app


