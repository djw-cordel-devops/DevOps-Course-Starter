class ViewModel:
    def __init__(self, items):
        self._items = items

## Index function returning list of all items
    @property
    def items(self):
        return self._items

##Retuning all items by status
    @property
    def get_wip_items(self):
        wip_items = []
        for item in self._items:
            if item.status == "Not Started":
                wip_items.append(item)
            if item.status == "In Progress":
                wip_items.append(item)
        return  wip_items