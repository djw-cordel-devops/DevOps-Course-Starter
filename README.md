# DevOps Apprenticeship: Project Exercise

## Creating the app using local environment / tools

**System Requirements**

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.7+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

**Poetry installation**

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
```

**Poetry installation (PowerShell)**

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py -UseBasicParsing).Content | python -
```

**Dependencies**

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

**Create a Trello account and API key**

We're going to be using Trello's API to fetch and save to-do tasks. 
1. Create an account by visiting this link https://trello.com/signup in you're favourite browser.
2. Generate an API Key and get your token
3. Update add the API_KEY and API_TOKEN to the `.env` file in your project folder.
4. On https://trello.com/ create a new board and give a title (e.g. todo_app).  
5. Add lists to your board by creating a three lists called "Not Strted","In Progress" and "Completed"
6. Once you have created your board you well need the BOARD_ID. To get the Board ID you need to use this API in POSTMAN https://api.trello.com/1/members/me/boards?fields=name,url&key={apiKey}&token={apiToken}
7. Update the BOARD_ID to the `.env` file in your project folder

**Running the App**

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

**Testing the app**

A number of test are included in test_app.py that validate the funcitonality of the todo.app.

- Item class test - Validates if item can be added
- ViewModel class tests - Valdiates that class can be used to add items
- MonkeyPatch test - Validate Index page with fake data / URL response

To excute the test - open a terminal and run 
```bash
peotry run pytest
```
## Using ansible to configure to deploy and run the application on AWS EC2

**General**
IDE such as VSCode
Putty or Keygen
Valid TRELLO_API_KEY
Valid TRELLO_TOKEN

**AWS Requirements**
To run the application on AWS EC2 requries a controller node and managed node - you willl need a minuim requirement of x2 EC2 instances in AWS.   

Please validate you have the IP addresses of both servers before moving onto next step.

**Configure ssh conenction on EC2 instances**

Configure local connection to the controller node usign keygen and use ```ssh-copy-id``` to copy this public key to the EC2 instance

 - Run ssh-keygen to generate an SSH key pair. This will generate it in the ec2-user's `.ssh` directory
 - Use the ssh-copy-id tool as before, but specifying the second VM's address this time

Validate ansible is installed on your controller node
```bash
ansible --version
```
If should provide information including version number
```bash
pip install ansible
```
Re-run ansible version to validate

**Source code requirements**  
Cooy todoapp from repo to the managed node into EC2 home directory under a new folder nammed todo_app - can be completed by downlading a zip file of the repo and using SCP to copy the folder contents or using vscode extension

**Using Ansible to configure you app servers**  
Open ssh session to EC2 controller node, from the EC2 console
```bash
ansible-playbook todo_app/ansible/todo_playbook.yml -i todo_app/ansible/inventory.ini
```
**Providing values and validation of sucessfully excution**  
So that app can be run sucessfully without exposing the values required - these are set varabiles that are declared when running the playbook.

Provide TRELLO_API_KEY when prompted  
Provide TRELLO_TOKEN when prompted

Validate playbook has run sucessfully - see below

PLAY RECAP *********************************************************************************************************************************************************************************************************************************************************************************************************
3.9.220.34                 : ok=9    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

**Validate App in runing on managed node**  
Open browser session and enter the IP and port number of 5000

## Running the app using Docker

### Using the app in Development:
```bash
docker build --target development --tag todo-app:dev . 
```
To run the docker file in dev mode:
```bash
docker run -d --env-file .\.env -p 5000:5000 --mount type=bind,source="$(pwd)"/todo_app,target=/app/todo_app todo-app:dev
```
Navigate to localhost:5000
### Using the app in Production:
```bash
docker build --target production --tag todo-app:prod . 
```
To run the docker file in prod mode:
```bash
docker run --env-file .\.env -p 8000:8000 todo-app:prod
```
Then navigate to http://0.0.0.0:8000/

### Running tests
```bash
docker compose up todo_app_tests --build
```

### Running the App using Docker compose
```bash
docker-compose up --build
```